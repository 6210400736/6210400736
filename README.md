##** สรุปแต่ละ commit

ครั้งที่ 1

   - เพิ่ม maven เข้าไปใน project

ครั้งที่ 2

   - เพิ่ม หน้าต่าง gui ทุกหน้า 
   - เพิ่ม Class Account
   - เชื่อมปุ่มกับController

ครั้งที่ 3

   - เพิ่ม polymorphism
   - แก้ไข method ใน Class
   - เพิ่มการทำงาน การตรวจสอบการเข้ารหัสว่าถูกต้องหรือไม่
   - เพิ่มการแจ้งเตือน เมื่อlogin ผิดพลาด
   - เพิ่มการทำงาน การสร้างบัญชีของ Staff และเพิ่มการแจ้งเตือน เมื่อมีการกรอกข้อมูลผิดพลาด
   - เพิ่ม Class letter ,Class Document ,Class Supplies, Class MailList
   - เพิ่ม ฟังก์ชัน Action จากหน้า admin กลับสู่หน้า login
   - เพิ่มการทำงานของการเปลี่ยนรหัสเปลี่ยนรหัสผ่าน

ครั้งที่ 4
    
   - เพิ่ม csv เข้าไปใน project
   - แก้ไข Constructor Class letter ,Class Document ,Class Supplies, Class MailList
   - แก้ไข หน้าต่าง gui admin

ครั้งที่ 5

   - เพิ่มหน้า gui room, resident
   - แก้ไขฟังก์ชัน StaffController , RegisterController, ControllerLogin
   - แก้ไขการแจ้งเตือน ของหน้า ChangePassword เมื่อใส่ข้อมูลผิดพลาด
   - แก้ไขฟังก์ชัน Class Account, Class Letter
   - แก้ไข Constructor Class letter ,Class Document ,Class Supplies, Class MailList
   
ครั้งที่ 6

   - เขียนไฟล์ Readme และ .PDF
   
ครั้งที่ 7

   - แก้ไข Class AccountFile
   - แก้ การตรวจสอบการ Login 
   - แก้ไขการทำงานของการ Ban Staff

ครั้งที่ 8
    
   - แก้ไข การทำงานการเปลี่ยนรหัสผ่าน
   
ครั้งที่ 9

   - เพิ่ม Class Room ในการเก็บข้อมูลห้องพัก
   - เพิ่ม RoomList ใช้สำหรับนำข้อมูลห้องพักไปสร้างเป็น ArrayList
   - RoomFile เพื่ออ่านและเขียนไฟล์ Room.csv
   - LetterFile เพื่ออ่านและเขียนไฟล์ Letter.csv
   - เพิ่ม TableView ใน StaffController เพื่อแสดงจดหมายนี่เข้ามา
   - เพิ่ม method สำหรับการเลือกรูป
   
ครั้งที่ 10
    
   - สร้างหน้า Gui การเพิ่ม Letter, Document, Supplies 
   - เพิ่ม MailList ใช้สำหรับนำข้อมูลของ จดหมาย, เอกสาร, พัสดุ ไปสร้างเป็น ArrayList
   - เพิ่ม method การจัดเรียง ตามเวลาที่จดหมายเข้า ใน MailList  
   - เพิ่มการทำงาน ในการสร้าง จดหมาย, เอกสาร, พัสดุ เข้าไปใน MailList
   - เพิ่ม MailReceviedController และ เพิ่ม TableView เพิ่อดูประวัติจดหมายที่ถูกรับไปแล้ว
   - เพิ่ม ResidentController และ เพิ่ม TableView เพื่อให้ผู้พักอาศัยเห็นจดหมายที่ตนยังไม่ได้รับ
   
ครั้งที่ 11

   - แก้ไข การทำงานในการเลือกรูป ของ จดหมาย เอกสาร พัสดุ
   - เพิ่ม การทำงาน ของการบอก การพยายาม Login ของ AccountStaff เมื่อถูก Ban  
   - เพิ่ม MailReceviedResidentController และ เพิ่ม TableView เพื่อให้ผู้พักอาศัยเห็นจดหมายที่ตนรับมาแล้ว
   - เพิ่ม RegisterResidentController เพื่อให้ ผู้พักอาศัยสามารถสร้าง Account  เพื่อเข้าไปดูจดหมาย ที่รับมาแล้ว และ ยังไม่ได้รับ 
   - เพิ่ม Interface Searcher เพิ่อนำไปใช้ในการ searcher
   - เพิ่ม Class ByPartialNameAndNumRoom เพื่อใช้ set ค่าที่ต้องการจะ searcher
   - เพิ่มการทำงาน searcher ในหน้า StaffController เพื่อ searcher หาข้อมูลตามเลขห้อง
   - เพิ่มการทำงาน searcher ในหน้า RoomListController เพื่อ searcher หาข้อมูลตามชื่อผู้พักอาศัย
   
##** การวางโครงสร้าง Directory

6210400736 -> data -> เก็บไฟล์ CSV

6210400736 -> src -> main -> java -> project -> controller -> เก็บตัว Controller ของแต่ละหน้า

6210400736 -> src -> main -> java -> project -> model -> เก็บ Class ทุก Class

6210400736 -> src -> main -> java -> project -> sevices -> เก็บClass ที่ใช้สำหรับเขียนและอ่าน File.csv

6210400736 -> src -> main -> java -> project -> strategy -> เก็บ Class ที่ใช้สำหรับการ searcher

6210400736 -> src -> main -> java -> project -> เก็บ MainApplication

6210400736 -> src -> main -> resources -> เก็บไฟล์ .fxml ทุกไฟล์

6210400736 -> src -> main -> resources -> image -> เก็บรูป icon ทุกอย่าง

6210400736 -> src -> main -> resources -> imageLetter -> เก็บรูป จดหมาย พัสดุ เอกสาร ทุกอัน

6210400736 -> MailBoxApplication -> 6210400736-jar.jar, data, images

6210400736 -> MailBoxApplication -> data -> เก็บไฟล์ CSV

6210400736 -> MailBoxApplication -> images -> เก็บรูปทุกอย่าง


##** วิธีการติดตั้งโปรแกรม

   - ไม่มีการติดตั้งส่วนต่างๆ

##** วิธีเปิดหน้าต่างโปรแกรม

   - เข้า Folder -> MailBoxApplication -> แล้ว Double click ไฟล์ 6210400736-jar.jar
   - หาก Double click ไม่ขึ้น ให้เปิด Command Prompt ขึ้นมา ทำการพิมพ์คำสั่ง java -jar แล้วทำการลากไฟล์ 6210400736-jar.jar เข้าไปใน Command Prompt แล้วกด Enter ก็จะสามารถเปิดหน้าต่างโปรแกรมได้

##** ตัวอย่าง username / password ใช้ทดสอบระบบ

   ** Account admin 
   
   - username = net / password = 1234
   
   ** Account staff 
   
   - username = staff / password = 1234
   
   ** Account Resident
   
   - username = re / password = 1234