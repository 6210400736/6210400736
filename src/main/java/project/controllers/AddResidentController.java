package project.controllers;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import project.model.AccountList;
import project.model.Room;
import project.model.RoomList;
import project.sevices.RoomFile;

import java.io.IOException;

public class AddResidentController {
    private AccountList accountList;
    private RoomList roomList;
    private RoomFile roomFile;
    private Room room;
    private String checkRoom = "don't have room";

    @FXML
    private Button backBtn, okBtn;
    @FXML
    private TextField nameResidentField, nameResidentSecondField;
    @FXML
    private ComboBox<String> building, floor, numRoom;

    @FXML
    public void initialize() {
        Platform.runLater(() -> {
            roomFile = new RoomFile("data");
            roomList = roomFile.getRoomsData("Room.csv");
        });
        building.getItems().addAll("A");
        floor.getItems().addAll("1", "2", "3", "4", "5", "6", "7", "8", "9");
        numRoom.getItems().addAll("01", "02", "03", "04", "05", "06", "07", "08", "09", "10");
    }

    @FXML
    public void handleOkBtnOnAction(ActionEvent event) throws IOException {
        if (building.getValue() == null || floor.getValue() == null || numRoom.getValue() == null || nameResidentField.getText().equals("")) {
            Alert a = new Alert(Alert.AlertType.WARNING);
            a.setTitle("เกิดข้อผิดพลาด");
            a.setContentText("กรุณากรอกข้อมูลให้ครบถ้วน");
            a.setHeaderText(null);
            a.showAndWait();
        } else if (building.getValue() != null && floor.getValue() != null && numRoom.getValue() != null && nameResidentField.getText() != "-") {
            for (Room room : roomList.toList()) {
                if (room.getFloor().equals(floor.getValue()) && room.getNumberRoom().equals(floor.getValue() + numRoom.getValue()) && room.getResidentName().equals("-") && room.getRoomType().equals("One Bedroom")) {
                    checkRoom = "have room";
                    room.setResidentName(nameResidentField.getText());
                    room.setResidentNameSecond("-");
                    roomFile.setRoomData(roomList);
                    Button okBtn = (Button) event.getSource();
                    Stage stage = (Stage) okBtn.getScene().getWindow();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/staff.fxml"));
                    stage.setScene(new Scene(loader.load(), 800, 600));
                    StaffController staffController = loader.getController();
                    staffController.setAcc(accountList);
                    StaffController staffController1 = loader.getController();
                    staffController1.setRoom(roomList);
                    stage.show();
                }
                if (room.getFloor().equals(floor.getValue()) && room.getNumberRoom().equals(floor.getValue() + numRoom.getValue()) && room.getResidentName().equals("-") && room.getRoomType().equals("Two Bedroom") && room.getResidentNameSecond().equals("-")) {
                    checkRoom = "have room";
                    room.setResidentName(nameResidentField.getText());
                    if (nameResidentSecondField.getText().equals("")) {
                        room.setResidentNameSecond("-");
                    } else {
                        room.setResidentNameSecond(nameResidentSecondField.getText());
                    }
                    roomFile.setRoomData(roomList);
                    Button okBtn = (Button) event.getSource();
                    Stage stage = (Stage) okBtn.getScene().getWindow();

                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/staff.fxml"));
                    stage.setScene(new Scene(loader.load(), 800, 600));
                    StaffController staffController = loader.getController();
                    staffController.setAcc(accountList);
                    StaffController staffController1 = loader.getController();
                    staffController1.setRoom(roomList);
                    stage.show();

                }
            }
            if (checkRoom.equals("don't have room")) {
                Alert a = new Alert(Alert.AlertType.WARNING);
                a.setTitle("เกิดข้อผิดพลาด");
                a.setContentText("ไม่พบห้อง หรือ มีผู้เข้าพักแล้ว");
                a.setHeaderText(null);
                a.showAndWait();
            }
        }
    }


    @FXML
    public void handleBackBtnOnAction(ActionEvent event) throws IOException {
        Button backBtn = (Button) event.getSource();
        Stage stage = (Stage) backBtn.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/staff.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        StaffController staffController = loader.getController();
        staffController.setAcc(accountList);

        stage.show();
    }

    public void setRoom(RoomList roomList) {
        this.roomList = roomList;
    }

    public void setAcc(AccountList accountList) {
        this.accountList = accountList;
    }
}
