package project.controllers;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.MenuButton;
import javafx.stage.Stage;
import project.model.AccountList;
import project.model.Room;
import project.model.RoomList;
import project.sevices.AccountFile;
import project.sevices.LetterFile;
import project.sevices.RoomFile;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AddRoomController {
    private AccountList accountList;
    private RoomList roomList;
    private RoomFile roomFile;
    private Room room;
    private String checkRoom = "don't have Room";
    @FXML
    private Button backBtn, okBtn;
    @FXML
    private ComboBox<String> building, floor, roomType, numRoom;

    @FXML
    public void initialize() {
        Platform.runLater(() -> {
            roomFile = new RoomFile("data");
            roomList = roomFile.getRoomsData("Room.csv");
        });
        building.getItems().addAll("A");
        floor.getItems().addAll("1", "2", "3", "4", "5", "6", "7", "8", "9");
        roomType.getItems().addAll("One Bedroom", "Two Bedroom");
        numRoom.getItems().addAll("01", "02", "03", "04", "05", "06", "07", "08", "09", "10");
    }

    public void handleBackBtnOnAction(ActionEvent event) throws IOException {
        Button backBtn = (Button) event.getSource();
        Stage stage = (Stage) backBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/staff.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        StaffController staffController = loader.getController();
        staffController.setAcc(accountList);
        stage.show();
    }

    public void handleOkBtnOnAction(ActionEvent event) throws IOException {
        room = new Room("-", "-", floor.getValue(), floor.getValue() + numRoom.getValue(), roomType.getValue(), building.getValue());
        for (Room room : roomList.toList()) {
            if (room.getNumberRoom().equals(floor.getValue() + numRoom.getValue()) && room.getFloor().equals(floor.getValue())) {
                checkRoom = "have Room";
            }
        }
        if (checkRoom == "don't have Room") {
            if (building.getValue() == null || floor.getValue() == null || roomType.getValue() == null || numRoom.getValue() == null) {
                Alert a = new Alert(Alert.AlertType.WARNING);
                a.setTitle("เกิดข้อผิดพลาด");
                a.setContentText("กรุณากรอกข้อมูลให้ครบถ้วน");
                a.setHeaderText(null);
                a.showAndWait();
            } else {
                roomList.addRoom(room);
                roomFile.setRoomData(roomList);
                Button okBtn = (Button) event.getSource();
                Stage stage = (Stage) okBtn.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/staff.fxml"));
                stage.setScene(new Scene(loader.load(), 800, 600));

                StaffController staffController = loader.getController();
                staffController.setAcc(accountList);
                StaffController staffController1 = loader.getController();
                staffController1.setRoom(roomList);
                checkRoom = "don't have Room";
                stage.show();
            }
        } else if (checkRoom == "have Room") {
            Alert a = new Alert(Alert.AlertType.WARNING);
            a.setTitle("เกิดข้อผิดพลาด");
            a.setContentText("มีห้องนี้แล้ว");
            a.setHeaderText(null);
            a.showAndWait();
        }
        checkRoom = "don't have Room";
    }

    public void setAcc(AccountList accountList) {
        this.accountList = accountList;
    }

    public void setRoom(RoomList roomList) {
        this.roomList = roomList;
    }
}
