package project.controllers;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import project.model.*;
import project.sevices.AccountFile;
import project.sevices.LetterFile;
import project.sevices.RoomFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

public class AddSuppliesController {
    private AccountList accountList;
    private MailList mailList;
    private LetterFile letterFile;
    private RoomList roomList;
    private RoomFile roomFile;
    private Room room;
    private Supplies supplies;
    private String path = "-";
    private String check = "don't have";
    @FXML
    private TextField senderNameField, recipientNameField, trackingNumberField;
    @FXML
    private ComboBox<String> deliveryService, building, floor, numRoom, size;
    ;
    @FXML
    private Button backBtn, okBtn, chooseImageBtn;
    @FXML
    private ImageView imageSupplies;

    @FXML
    public void initialize() {
        Platform.runLater(() -> {
            letterFile = new LetterFile("data");
            roomFile = new RoomFile("data");
            roomList = roomFile.getRoomsData("Room.csv");
        });
        deliveryService.getItems().addAll("Kerry", "PostThailand");
        building.getItems().addAll("A");
        floor.getItems().addAll("1", "2", "3", "4", "5", "6", "7", "8", "9");
        numRoom.getItems().addAll("01", "02", "03", "04", "05", "06", "07", "08", "09", "10");
        size.getItems().addAll("Large", "Middle", "Small");
    }

    @FXML
    public void handleOkBtnOnAction(ActionEvent event) throws IOException {
        supplies = new Supplies(senderNameField.getText(), recipientNameField.getText(), floor.getValue() + numRoom.getValue(), "Not received", size.getValue(), "Supplies", "-", "-", path, "-", "-", trackingNumberField.getText(), deliveryService.getValue());
        if (senderNameField.getText().equals("") || recipientNameField.getText().equals("") || floor.getValue() == null || numRoom.getValue() == null || size.getValue() == null || trackingNumberField.getText().equals("") || deliveryService.getValue() == null) {
            Alert a = new Alert(Alert.AlertType.WARNING);
            a.setTitle("เกิดข้อผิดพลาด");
            a.setContentText("กรุณากรอกข้อมูลให้ครบถ้วน");
            a.setHeaderText(null);
            a.showAndWait();
        } else if (supplies.getImagePath().equals("-")) {
            Alert a = new Alert(Alert.AlertType.WARNING);
            a.setTitle("เกิดข้อผิดพลาด");
            a.setContentText("กรุณาใส่รูปพัสดุของท่าน");
            a.setHeaderText(null);
            a.showAndWait();
        } else if (senderNameField.getText() != "" && recipientNameField.getText() != "" && floor.getValue() != null && numRoom.getValue() != null && size.getValue() != null && trackingNumberField.getText() != "" && deliveryService.getValue() != null) {
            for (Room room : roomList.toList()) {
                if (room.getNumberRoom().equals(floor.getValue() + numRoom.getValue()) && (room.getResidentName().equals(recipientNameField.getText()) || room.getResidentNameSecond().equals(recipientNameField.getText()))) {
                    check = "have";
                }
            }
            if (check.equals("have")) {
                letterFile = new LetterFile("data");
                supplies.setStaffAdd(accountList.getCurrentAccount().getName());
                mailList.addMailSupplies(supplies);
                Date date = new Date();
                SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
                String strDate = formatter.format(date);
                supplies.setTimeIn(strDate);
                letterFile.setLetterData(mailList);
                Button okBtn = (Button) event.getSource();
                Stage stage = (Stage) okBtn.getScene().getWindow();

                FXMLLoader loader = new FXMLLoader(getClass().getResource("/staff.fxml"));
                stage.setScene(new Scene(loader.load(), 800, 600));
                StaffController staffController = loader.getController();
                staffController.setAcc(accountList);
                StaffController staffController1 = loader.getController();
                staffController1.setMailList(mailList);

                stage.show();
            } else if (check.equals("don't have")) {
                Alert a = new Alert(Alert.AlertType.WARNING);
                a.setTitle("เกิดข้อผิดพลาด");
                a.setContentText("ข้อมูลไม่ตรงกับห้องพัก กรุณาตรวจสอบ ชื่อผู้รับ ชั้น เลขห้อง");
                a.setHeaderText(null);
                a.showAndWait();
            }
        }
    }


    @FXML
    public void handleBackBtnOnAction(ActionEvent event) throws IOException {
        Button backBtn = (Button) event.getSource();
        Stage stage = (Stage) backBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/letter.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        LetterController letterController = loader.getController();
        letterController.setAcc(accountList);
        LetterController letterController1 = loader.getController();
        letterController1.setMailList(mailList);

        stage.show();
    }

    @FXML
    public void handleChooseImageBtnOnAction(ActionEvent event) throws IOException {
        FileChooser chooser = new FileChooser();
        chooser.setInitialDirectory(new File(System.getProperty("user.dir")));
        chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("images PNG JPG", "*.png", "*.jpg"));
        Button b = (Button) event.getSource();
        File file = chooser.showOpenDialog(b.getScene().getWindow());
        if (file != null) {
            try {
                File destDir = new File("images");
                destDir.mkdirs();

                String[] fileSplit = file.getName().split(File.separator + "\\.");
                String filename = LocalDate.now() + "_" + System.currentTimeMillis() + "." + fileSplit[fileSplit.length - 1];
                String filePath = "images" + File.separator + filename;
                Files.copy(file.toPath(), new File(filePath).toPath(), StandardCopyOption.REPLACE_EXISTING);
                imageSupplies.setImage(new Image(new File(filePath).toURI().toString()));
                this.path = filePath;
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public void setAcc(AccountList accountList) {
        this.accountList = accountList;
    }

    public void setMailList(MailList mailList) {
        this.mailList = mailList;
    }

    public void setRoom(RoomList roomList) {
        this.roomList = roomList;
    }

}
