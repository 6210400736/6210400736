package project.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import project.model.Account;
import project.model.AccountList;
import project.sevices.AccountFile;
import project.sevices.StringConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class AdminController {

    private Account selectedAccount;
    private Account account;
    private AccountFile dataSource;
    private AccountList accountList;
    private ObservableList<Account> accountObservableList;

    @FXML
    private Button addBtn, susPenBtn, changPassBtn, logoutBtn;
    @FXML
    private Label userLabel;
    @FXML
    private Label nameLabel;
    @FXML
    private ImageView imageStaff;
    @FXML
    private TableView<Account> staffTable;

    @FXML
    public void initialize() {
        dataSource = new AccountFile("data");
        susPenBtn.setDisable(true);
        accountList = dataSource.getAccountsData("Account.csv");
        showAccountData();
        staffTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                showSelectedAccount(newValue);
            }
        });
    }

    private void showAccountData() {
        accountObservableList = FXCollections.observableArrayList(accountList.toStaffList());
        staffTable.setItems(accountObservableList);

        ArrayList<StringConfiguration> configs = new ArrayList<>();
        configs.add(new StringConfiguration("title:Name", "field:name"));
        configs.add(new StringConfiguration("title:UserName", "field:userName"));
        configs.add(new StringConfiguration("title:Status", "field:status"));
        configs.add(new StringConfiguration("title:Online", "field:onlineTime"));
        configs.add(new StringConfiguration("title:TryToLogin", "field:login"));

        for (StringConfiguration conf : configs) {
            TableColumn col = new TableColumn(conf.get("title"));
            col.setCellValueFactory(new PropertyValueFactory<>(conf.get("field")));
            col.prefWidthProperty().bind(staffTable.widthProperty().divide(5));
            staffTable.getColumns().add(col);
        }

    }

    private void showSelectedAccount(Account accountStaff) {
        selectedAccount = accountStaff;
        userLabel.setText(accountStaff.getUserName());
        nameLabel.setText(accountStaff.getName());
        imageStaff.setImage(new Image(new File(selectedAccount.getImagePath()).toURI().toString()));
        susPenBtn.setDisable(false);
    }

    private void clearSelectedAccount() {
        selectedAccount = null;
        userLabel.setText("...");
        nameLabel.setText("...");
        imageStaff.setImage(new Image("image/User.png"));
        susPenBtn.setDisable(true);
    }

    @FXML
    public void handleSusPenBtnOnAction(ActionEvent event) throws IOException {
        for (Account account : accountList.toList()) {
            if (selectedAccount.getStatus().equals("Normal") && account.getUserName().equals(selectedAccount.getUserName())) {
                selectedAccount.setStatus("ban");
                account.setStatus("ban");
            } else if (selectedAccount.getStatus().equals("ban") && account.getUserName().equals(selectedAccount.getUserName())) {
                selectedAccount.setStatus("Normal");
                selectedAccount.setLogin("0");
                account.setLogin("0");
                account.setStatus("Normal");

            }
        }
        clearSelectedAccount();
        staffTable.refresh();
        staffTable.getSelectionModel().clearSelection();
        dataSource.setAccountData(accountList);

    }


    @FXML
    public void handleLogoutBtnOnAction(ActionEvent event) throws IOException {
        Button logoutBtn = (Button) event.getSource();
        Stage stage = (Stage) logoutBtn.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/login.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        ControllerLogin controllerLogin = loader.getController();
        controllerLogin.setAcc(accountList);
        stage.show();
    }

    @FXML
    public void handleAddBtnOnAction(ActionEvent event) throws IOException {
        Button addBtn = (Button) event.getSource();
        Stage stage = (Stage) addBtn.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/registerStaff.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        RegisterStaffController registerController = loader.getController();
        registerController.setAcc(accountList);
        stage.show();
    }

    @FXML
    public void handleChangePasswordBtnOnAction(ActionEvent event) throws IOException {
        Button changPassBtn = (Button) event.getSource();
        Stage stage = (Stage) changPassBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/changepassword.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        ChangePasswordController changePasswordController = loader.getController();
        changePasswordController.setAcc(accountList);
        stage.show();
    }

    public void setAcc(AccountList accountList) {
        this.accountList = accountList;
    }

}
