package project.controllers;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.stage.Stage;
import project.model.AccountList;
import project.sevices.AccountFile;

import java.io.IOException;

public class ChangePasswordController {
    private AccountFile dataSource;
    private AccountList accountList;
    @FXML
    private Button okBtn, backBtn;
    @FXML
    private PasswordField oldPassField, newPassField, conPassField;


    @FXML
    public void initialize() {
        Platform.runLater(() -> {
            dataSource = new AccountFile("data");
        });
    }

    @FXML
    public void handleBackBtnOnAction(ActionEvent event) throws IOException {
        if (accountList.getCurrentAccount().getType().equals("Admin")) {
            Button backBtn = (Button) event.getSource();
            Stage stage = (Stage) backBtn.getScene().getWindow();

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/admin.fxml"));
            stage.setScene(new Scene(loader.load(), 800, 600));
            AdminController adminController = loader.getController();
            adminController.setAcc(accountList);
            stage.show();
        }
        if (accountList.getCurrentAccount().getType().equals("Staff")) {
            Button backBtn = (Button) event.getSource();
            Stage stage = (Stage) backBtn.getScene().getWindow();

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/staff.fxml"));
            stage.setScene(new Scene(loader.load(), 800, 600));
            StaffController staffController = loader.getController();
            staffController.setAcc(accountList);
            stage.show();
        }
        if (accountList.getCurrentAccount().getType().equals("Resident")) {
            Button backBtn = (Button) event.getSource();
            Stage stage = (Stage) backBtn.getScene().getWindow();

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/resident.fxml"));
            stage.setScene(new Scene(loader.load(), 800, 600));
            ResidentController residentController = loader.getController();
            residentController.setAcc(accountList);
            stage.show();
        }
    }

    @FXML
    public void handleOkBtnOnAction(ActionEvent event) throws IOException {
        System.out.println(accountList.getCurrentAccount());
        if (newPassField.getText().equals("") || conPassField.getText().equals("") || oldPassField.getText().equals("")) {
            Alert a = new Alert(Alert.AlertType.WARNING);
            a.setTitle("เกิดข้อผิดพลาด");
            a.setContentText("กรุณากรอกข้อมูลให้ครบถ้วน");
            a.setHeaderText(null);
            a.showAndWait();
        } else if (oldPassField.getText().equals(accountList.getCurrentAccount().getPassword()) && conPassField.getText().equals(newPassField.getText())) {
            accountList.getCurrentAccount().setPassword(newPassField.getText());
            dataSource.setAccountData(accountList);
            Button okBtn = (Button) event.getSource();
            Stage stage = (Stage) okBtn.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/login.fxml"));
            stage.setScene(new Scene(loader.load(), 800, 600));
            ControllerLogin controllerLogin = loader.getController();
            controllerLogin.setAcc(accountList);
            stage.show();
        } else {
            Alert a = new Alert(Alert.AlertType.WARNING);
            a.setTitle("เกิดข้อผิดพลาด");
            a.setContentText("รหัสผ่านไม่ตรงกัน");
            a.setHeaderText(null);
            a.showAndWait();
        }

    }

    public void setAcc(AccountList accountList) {
        this.accountList = accountList;
    }
}
