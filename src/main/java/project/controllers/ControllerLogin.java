package project.controllers;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import project.model.Account;
import project.model.AccountList;
import project.model.RoomList;
import project.sevices.AccountFile;
import project.sevices.LetterFile;
import project.sevices.RoomFile;


import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class ControllerLogin {

    private AccountList accountList;
    private RoomList roomList;
    private RoomFile roomFile;
    private AccountFile dataSource;
    private String checkState = "can't login";

    @FXML
    private Button infoBtn, helpBtn, loginBtn, registerBtn;
    @FXML
    private PasswordField passwordField;
    @FXML
    private TextField userField;


    @FXML
    public void initialize() {
        dataSource = new AccountFile("data");
    }


    @FXML
    public void handleLoginBtnOnAction(ActionEvent event) throws IOException {
        System.out.println(userField.getText());
        System.out.println(passwordField.getText());
        AccountList accountList = new AccountList();
        dataSource.accountList(accountList);
        for (Account account : accountList.toList()) {
            if (userField.getText().equals(account.getUserName()) && account.getStatus().equals("ban")) {
                checkState = "ban";
                break;
            }
            if (passwordField.getText().equals(account.getPassword()) && userField.getText().equals(account.getUserName())) {
                if (account.getType().equals("Admin")) {
                    accountList.checkPasswordAndUserName(passwordField.getText(), userField.getText());
                    Button loginBtn = (Button) event.getSource();
                    Stage stage = (Stage) loginBtn.getScene().getWindow();

                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/admin.fxml"));
                    stage.setScene(new Scene(loader.load(), 800, 600));

                    AdminController adminController = loader.getController();
                    adminController.setAcc(accountList);

                    checkState = "login";
                    stage.show();

                }
                if (account.getType().equals("Staff")) {
                    if (account.getStatus().equals("Normal")) {
                        accountList.checkPasswordAndUserName(passwordField.getText(), userField.getText());
                        Date date = new Date();
                        SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
                        String strDate = formatter.format(date);
                        account.setOnlineTime(strDate);
                        dataSource.setAccountData(accountList);
                        Button loginBtn = (Button) event.getSource();
                        Stage stage = (Stage) loginBtn.getScene().getWindow();

                        FXMLLoader loader = new FXMLLoader(getClass().getResource("/staff.fxml"));
                        stage.setScene(new Scene(loader.load(), 800, 600));

                        StaffController staffController = loader.getController();
                        staffController.setAcc(accountList);
                        checkState = "login";
                        stage.show();
                    }
                }
                if (account.getType().equals("Resident")) {
                    if (account.getStatus().equals("Normal")) {
                        accountList.checkPasswordAndUserName(passwordField.getText(), userField.getText());
                        dataSource.setAccountData(accountList);
                        Button loginBtn = (Button) event.getSource();
                        Stage stage = (Stage) loginBtn.getScene().getWindow();

                        FXMLLoader loader = new FXMLLoader(getClass().getResource("/resident.fxml"));
                        stage.setScene(new Scene(loader.load(), 800, 600));

                        ResidentController residentController = loader.getController();
                        residentController.setAcc(accountList);
                        checkState = "login";
                        stage.show();
                    }
                }
            }
        }
        if (checkState.equals("ban")) {
            Alert a = new Alert(Alert.AlertType.WARNING);
            a.setTitle("เกิดข้อผิดพลาด");
            a.setContentText("ถูกระงับการใช้งาน");
            a.setHeaderText(null);
            a.showAndWait();
            if (accountList.checkPasswordAndUserName(passwordField.getText(), userField.getText())) {
                int count = Integer.parseInt(accountList.getCurrentAccount().getLogin());
                count += 1;
                accountList.getCurrentAccount().setLogin(String.valueOf(count));
                dataSource.setAccountData(accountList);
            }
        } else if (checkState.equals("can't login")) {
            Alert a = new Alert(Alert.AlertType.WARNING);
            a.setTitle("เกิดข้อผิดพลาด");
            a.setContentText("ชื่อผู้ใช้ และ/หรือ รหัสผ่าน ไม่ถูกต้อง");
            a.setHeaderText(null);
            a.showAndWait();
        }
        checkState = "can't login";
    }


    @FXML
    public void handleRegisterBtnOnAction(ActionEvent event) throws IOException {
        Button registerBtn = (Button) event.getSource();
        Stage stage = (Stage) registerBtn.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/registerResident.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));

        stage.show();
    }

    @FXML
    public void handleInfoBtnOnAction(ActionEvent event) throws IOException {
        Button infoBtn = (Button) event.getSource();
        Stage stage = (Stage) infoBtn.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/information.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));

        stage.show();
    }


    @FXML
    public void handleHelpBtnOnAction(ActionEvent event) throws IOException {
        Button helpBtn = (Button) event.getSource();
        Stage stage = (Stage) helpBtn.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/help.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));

        stage.show();
    }

    public void setAcc(AccountList accountList) {
        this.accountList = accountList;
    }
}
