package project.controllers;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;

public class HelpController {

    @FXML
    private VBox vBox;
    @FXML
    private Button backBtn;

    @FXML
    public void initialize() {
        Platform.runLater(() -> {
            for (int i = 1; i < 22; i++) {
                String image = "/helpPic/page-" + i + ".jpg";
                ImageView imageView = new ImageView(new Image(image));
                vBox.getChildren().add(imageView);
            }
        });
    }

    @FXML
    public void handleBackBtnOnAction(ActionEvent event) throws IOException {
        Button helpBtn = (Button) event.getSource();
        Stage stage = (Stage) helpBtn.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/login.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));

        stage.show();
    }
}
