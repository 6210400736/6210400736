package project.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import project.model.AccountList;
import project.model.MailList;

import java.io.IOException;

public class LetterController {
    private AccountList accountList;
    private MailList mailList;
    @FXML
    private Button backBtn, letterBtn, documentBtn, suppliesBtn;

    @FXML
    public void handleBackBtnOnAction(ActionEvent event) throws IOException {

        Button backBtn = (Button) event.getSource();
        Stage stage = (Stage) backBtn.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/staff.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        StaffController staffController = loader.getController();
        staffController.setAcc(accountList);
        StaffController staffController1 = loader.getController();
        staffController1.setMailList(mailList);
        stage.show();
    }

    @FXML
    public void handleLetterBtnOnAction(ActionEvent event) throws IOException {

        Button letterBtn = (Button) event.getSource();
        Stage stage = (Stage) letterBtn.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/addletter.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        AddLetterController addLetterController = loader.getController();
        addLetterController.setAcc(accountList);
        AddLetterController addLetterController1 = loader.getController();
        addLetterController1.setMailList(mailList);
        stage.show();
    }

    @FXML
    public void handleDocumentBtnOnAction(ActionEvent event) throws IOException {

        Button documentBtn = (Button) event.getSource();
        Stage stage = (Stage) documentBtn.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/adddocument.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        AddDocumentController addDocumentController = loader.getController();
        addDocumentController.setAcc(accountList);
        AddDocumentController addDocumentController1 = loader.getController();
        addDocumentController1.setMailList(mailList);
        stage.show();
    }

    @FXML
    public void handleSuppliesBtnOnAction(ActionEvent event) throws IOException {

        Button suppliesBtn = (Button) event.getSource();
        Stage stage = (Stage) suppliesBtn.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/addSupplies.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        AddSuppliesController addSuppliesController = loader.getController();
        addSuppliesController.setAcc(accountList);
        AddSuppliesController addSuppliesController1 = loader.getController();
        addSuppliesController1.setMailList(mailList);
        stage.show();
    }


    public void setAcc(AccountList accountList) {
        this.accountList = accountList;
    }

    public void setMailList(MailList mailList) {
        this.mailList = mailList;
    }
}
