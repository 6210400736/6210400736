package project.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import project.model.Account;
import project.model.AccountList;
import project.model.Letter;
import project.model.MailList;
import project.sevices.AccountFile;
import project.sevices.LetterFile;
import project.sevices.StringConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class MailReceivedController {
    private AccountList accountList;
    private Account account;
    private LetterFile letterFile;
    private MailList mailList;
    private Letter selectedLetter;
    private ObservableList<Letter> lettersObservableList;

    @FXML
    private Button backBtn;
    @FXML
    private Label senderNameLabel, recipientName, numberRoomLabel, typeLabel, staffNameLabel;
    @FXML
    private TableView<Letter> letterReceivedTable;
    @FXML
    private ImageView imagePath;


    @FXML
    public void initialize() {
        letterFile = new LetterFile("data");
        mailList = letterFile.getLetterData("Letter.csv");
        showAccountData();
        letterReceivedTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                showSelectedLetter(newValue);
            }
        });
    }

    private void showAccountData() {
        lettersObservableList = FXCollections.observableArrayList(mailList.letterReceivedList());
        letterReceivedTable.setItems(lettersObservableList);

        ArrayList<StringConfiguration> configs = new ArrayList<>();
        configs.add(new StringConfiguration("title:ชื่อผู้ส่ง", "field:senderName"));
        configs.add(new StringConfiguration("title:ชื่อผู้รับ", "field:recipientName"));
        configs.add(new StringConfiguration("title:เจ้าหน้าที่รับจดหมาย", "field:staffPick"));
        configs.add(new StringConfiguration("title:ประเภท", "field:type"));
        configs.add(new StringConfiguration("title:สถานะ", "field:status"));
        configs.add(new StringConfiguration("title:เวลาที่รับจดหมาย", "field:timeOut"));

        for (StringConfiguration conf : configs) {
            TableColumn col = new TableColumn(conf.get("title"));
            col.setCellValueFactory(new PropertyValueFactory<>(conf.get("field")));
            col.prefWidthProperty().bind(letterReceivedTable.widthProperty().divide(6));
            letterReceivedTable.getColumns().add(col);
        }

    }

    private void showSelectedLetter(Letter letter) {
        selectedLetter = letter;
        senderNameLabel.setText(letter.getSenderName());
        recipientName.setText(letter.getRecipientName());
        numberRoomLabel.setText(letter.getRoomNumber());
        typeLabel.setText(letter.getType());
        staffNameLabel.setText(letter.getStaffPick());
        imagePath.setImage(new Image(new File(selectedLetter.getImagePath()).toURI().toString()));
    }


    @FXML
    public void handleBackBtnOnAction(ActionEvent event) throws IOException {
        Button backBtn = (Button) event.getSource();
        Stage stage = (Stage) backBtn.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/staff.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        StaffController staffController1 = loader.getController();
        staffController1.setAcc(accountList);
        stage.show();
    }

    public void setAcc(AccountList accountList) {
        this.accountList = accountList;
    }
}
