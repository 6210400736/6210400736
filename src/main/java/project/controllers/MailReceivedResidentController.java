package project.controllers;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import project.model.AccountList;
import project.model.Letter;
import project.model.MailList;
import project.sevices.AccountFile;
import project.sevices.LetterFile;
import project.sevices.RoomFile;
import project.sevices.StringConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class MailReceivedResidentController {
    private AccountList accountList;
    private LetterFile letterFile;
    private Letter selectedLetter;
    private MailList mailList;
    private AccountFile accountFile;
    private ObservableList<Letter> lettersObservableList;

    @FXML
    private Label userNameLabel, nameLabel, senderNameLabel, recipientName, staffPickLabel, typeLabel, timeLabel, staffAddLabel;
    @FXML
    private TableView<Letter> letterReceivedTable;
    @FXML
    private Button logoutBtn, changePassBnt, mailReceivedBtn;
    @FXML
    private ImageView imagePath;

    @FXML
    private void initialize() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                showLetterData();
            }
        });
        letterFile = new LetterFile("data");
        mailList = letterFile.getLetterData("Letter.csv");
        letterReceivedTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                showSelectedLetter(newValue);
            }
        });

    }

    private void showLetterData() {
        ArrayList<Letter> newList = new ArrayList<>();
        for (Letter letter : mailList.letterReceivedList()) {
            if (accountList.getCurrentAccount().getNumRoom().equals(letter.getRoomNumber())) {
                newList.add(letter);
            }
        }
        lettersObservableList = FXCollections.observableArrayList(newList);
        letterReceivedTable.setItems(lettersObservableList);

        ArrayList<StringConfiguration> configs = new ArrayList<>();
        configs.add(new StringConfiguration("title:ชื่อผู้ส่ง", "field:senderName"));
        configs.add(new StringConfiguration("title:ชื่อผู้รับ", "field:recipientName"));
        configs.add(new StringConfiguration("title:เจ้าหน้าที่นำจดหมายเข้า", "field:staffAdd"));
        configs.add(new StringConfiguration("title:เจ้าหน้าที่รับจดหมาย", "field:staffPick"));
        configs.add(new StringConfiguration("title:สถานะ", "field:status"));
        configs.add(new StringConfiguration("title:ขนาด", "field:size"));
        configs.add(new StringConfiguration("title:ประเภท", "field:type"));
        configs.add(new StringConfiguration("title:เลขห้อง", "field:roomNumber"));
        configs.add(new StringConfiguration("title:เวลาที่จดหมายเข้า", "field:timeIn"));

        for (StringConfiguration conf : configs) {
            TableColumn col = new TableColumn(conf.get("title"));
            col.setCellValueFactory(new PropertyValueFactory<>(conf.get("field")));
            col.prefWidthProperty().bind(letterReceivedTable.widthProperty().divide(7));
            letterReceivedTable.getColumns().add(col);
        }
    }

    private void showSelectedLetter(Letter letter) {
        selectedLetter = letter;
        senderNameLabel.setText(letter.getSenderName());
        recipientName.setText(letter.getRecipientName());
        staffPickLabel.setText(letter.getStaffPick());
        staffAddLabel.setText(letter.getStaffAdd());
        typeLabel.setText(letter.getType());
        timeLabel.setText(letter.getTimeOut());
        imagePath.setImage(new Image(new File(selectedLetter.getImagePath()).toURI().toString()));
    }

    @FXML
    public void handleBackBtnOnAction(ActionEvent event) throws IOException {
        Button backBtn = (Button) event.getSource();
        Stage stage = (Stage) backBtn.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/resident.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        ResidentController residentController = loader.getController();
        residentController.setAcc(accountList);
        stage.show();
    }

    public void setAcc(AccountList accountList) {
        this.accountList = accountList;
    }
}
