package project.controllers;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import project.model.Account;
import project.model.AccountList;
import project.model.Room;
import project.model.RoomList;
import project.sevices.AccountFile;
import project.sevices.RoomFile;

import java.io.IOException;

public class RegisterResidentController {
    private RoomFile roomFile;
    private RoomList roomList;
    private AccountFile accountFile;
    private AccountList accountList;
    private String checkUserName = "don't have Username", checkRoom = "don't have room";
    private String path;

    @FXML
    private Button okBtn, backBtn, chooseImageBtn;
    @FXML
    private TextField nameField, userNameField, emailField;
    @FXML
    private PasswordField passField, conPassField;
    @FXML
    private ComboBox<String> building, floor, numRoom;

    @FXML
    public void initialize() {
        Platform.runLater(() -> {
            accountFile = new AccountFile("data");
            accountList = accountFile.getAccountsData("Account.csv");
            roomFile = new RoomFile("data");
            roomList = roomFile.getRoomsData("Room.csv");
        });
        building.getItems().addAll("A");
        floor.getItems().addAll("1", "2", "3", "4", "5", "6", "7", "8", "9");
        numRoom.getItems().addAll("01", "02", "03", "04", "05", "06", "07", "08", "09", "10");
    }

    @FXML
    public void handleOkBtnOnAction(ActionEvent event) throws IOException {
        System.out.println(userNameField.getText());
        System.out.println(passField.getText());
        Account resident = new Account(userNameField.getText(), nameField.getText(), emailField.getText(), passField.getText(), "Resident", "Normal", "-", path, "-", "0");
        for (Account account : accountList.toList()) {
            if (userNameField.getText().equals(account.getUserName())) {
                checkUserName = "have username";
            }
        }
        if (building.getValue() == null || floor.getValue() == null || numRoom.getValue() == null || userNameField.getText().equals("") || nameField.getText().equals("") || emailField.getText().equals("") || passField.getText().equals("") || conPassField.getText().equals("")) {
            Alert a = new Alert(Alert.AlertType.WARNING);
            a.setTitle("เกิดข้อผิดพลาด");
            a.setContentText("กรุณากรอกข้อมูลให้ครบถ้วน");
            a.setHeaderText(null);
            a.showAndWait();
        }
        if (building.getValue() != null && floor.getValue() != null && numRoom.getValue() != null && userNameField.getText() != "" && nameField.getText() != "" && emailField.getText() != "" && passField.getText() != "" && conPassField.getText() != "" && checkUserName.equals("don't have Username")) {
            for (Room room : roomList.toList()) {
                if ((room.getFloor().equals(floor.getValue()) && room.getNumberRoom().equals(floor.getValue() + numRoom.getValue())) && (room.getResidentName().equals(nameField.getText()) || room.getResidentNameSecond().equals(nameField.getText()))) {
                    checkRoom = "have room";
                    if (checkRoom.equals("have room")) {
                        if (conPassField.getText().equals(passField.getText())) {

                            resident.setNumRoom(floor.getValue() + numRoom.getValue());
                            accountList.addAccount(resident);
                            accountFile.setAccountData(accountList);
                            Button okBtn = (Button) event.getSource();
                            Stage stage = (Stage) okBtn.getScene().getWindow();
                            FXMLLoader loader = new FXMLLoader(getClass().getResource("/login.fxml"));
                            stage.setScene(new Scene(loader.load(), 800, 600));
                            ControllerLogin controllerLogin = loader.getController();
                            controllerLogin.setAcc(accountList);
                            stage.show();

                        } else {
                            Alert a = new Alert(Alert.AlertType.WARNING);
                            a.setTitle("เกิดข้อผิดพลาด");
                            a.setContentText("รหัสผ่านไม่ตรงกัน");
                            a.setHeaderText(null);
                            a.showAndWait();

                        }
                    }
                }
            }
            if (checkRoom.equals("don't have room")) {
                Alert a = new Alert(Alert.AlertType.WARNING);
                a.setTitle("เกิดข้อผิดพลาด");
                a.setContentText("ข้อมูลไม่ตรงกับระบบ");
                a.setHeaderText(null);
                a.showAndWait();
            }
        } else if (checkUserName.equals("have username")) {
            Alert a = new Alert(Alert.AlertType.WARNING);
            a.setTitle("เกิดข้อผิดพลาด");
            a.setContentText("ชื่อผู้ใช้ซ้ำกัน");
            a.setHeaderText(null);
            a.showAndWait();
        }

        checkUserName = "don't have Username";

    }

    @FXML
    public void handleBackBtnOnAction(ActionEvent event) throws IOException {
        Button backBtn = (Button) event.getSource();
        Stage stage = (Stage) backBtn.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/login.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        ControllerLogin controllerLogin = loader.getController();
        controllerLogin.setAcc(accountList);

        stage.show();
    }
}
