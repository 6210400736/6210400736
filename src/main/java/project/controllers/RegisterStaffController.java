package project.controllers;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import project.model.Account;
import project.model.AccountList;
import project.sevices.AccountFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;

public class RegisterStaffController {
    private AccountList accountList;
    private AccountFile dataSource;
    private String path = "-";
    private String checkUserName = "don't have Username";

    @FXML
    private Button okBtn, backBtn, chooseImageBtn;
    @FXML
    private
    TextField userNameField, nameField, emailField;
    @FXML
    private
    PasswordField passField, conPassField;
    @FXML
    private
    ImageView iconRegister;

    @FXML
    public void initialize() {
        Platform.runLater(() -> {
            dataSource = new AccountFile("data");
        });
    }

    @FXML
    public void handleBackBtnOnAction(ActionEvent event) throws IOException {
        System.out.println(accountList);
        System.out.println(accountList.getCurrentAccount());
        Button backBtn = (Button) event.getSource();
        Stage stage = (Stage) backBtn.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/admin.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        AdminController adminController = loader.getController();
        adminController.setAcc(accountList);
        stage.show();
    }

    @FXML
    public void handleOkBtnOnAction(ActionEvent event) throws IOException {
        System.out.println(userNameField.getText());
        System.out.println(passField.getText());
        Account staff = new Account(userNameField.getText(), nameField.getText(), emailField.getText(), passField.getText(), "Staff", "Normal", "-", path, "-", "0");
        for (Account account : accountList.toList()) {
            if (userNameField.getText().equals(account.getUserName())) {
                checkUserName = "have username";
            }
        }
        if (checkUserName.equals("don't have Username")) {
            if (userNameField.getText().equals("") || nameField.getText().equals("") || emailField.getText().equals("") || passField.getText().equals("") || conPassField.getText().equals("")) {
                Alert a = new Alert(Alert.AlertType.WARNING);
                a.setTitle("เกิดข้อผิดพลาด");
                a.setContentText("กรุณากรอกข้อมูลให้ครบถ้วน");
                a.setHeaderText(null);
                a.showAndWait();
            } else if (staff.getImagePath().equals("-")) {
                Alert a = new Alert(Alert.AlertType.WARNING);
                a.setTitle("เกิดข้อผิดพลาด");
                a.setContentText("กรุณาใส่รูปของเจ้าหน้าที่");
                a.setHeaderText(null);
                a.showAndWait();
            } else if (conPassField.getText().equals(passField.getText())) {
                dataSource = new AccountFile("data");
                accountList.addAccount(staff);
                dataSource.setAccountData(accountList);
                Button okBtn = (Button) event.getSource();
                Stage stage = (Stage) okBtn.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/admin.fxml"));
                stage.setScene(new Scene(loader.load(), 800, 600));
                AdminController adminController = loader.getController();
                adminController.setAcc(accountList);
                checkUserName = "don't have Username";
                stage.show();

            } else {
                Alert a = new Alert(Alert.AlertType.WARNING);
                a.setTitle("เกิดข้อผิดพลาด");
                a.setContentText("รหัสผ่านไม่ตรงกัน");
                a.setHeaderText(null);
                a.showAndWait();

            }
        } else if (checkUserName.equals("have username")) {
            Alert a = new Alert(Alert.AlertType.WARNING);
            a.setTitle("เกิดข้อผิดพลาด");
            a.setContentText("ชื่อผู้ใช้ซ้ำกัน");
            a.setHeaderText(null);
            a.showAndWait();
        }
        checkUserName = "don't have Username";

    }

    @FXML
    public void handleChooseImageBtnOnAction(ActionEvent event) throws IOException {
        FileChooser chooser = new FileChooser();
        chooser.setInitialDirectory(new File(System.getProperty("user.dir")));
        chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("images PNG JPG", "*.png", "*.jpg"));
        Button b = (Button) event.getSource();
        File file = chooser.showOpenDialog(b.getScene().getWindow());
        if (file != null) {
            try {
                File destDir = new File("images");
                destDir.mkdirs();

                String[] fileSplit = file.getName().split("\\.");
                String filename = LocalDate.now() + "_" + System.currentTimeMillis() + "." + fileSplit[fileSplit.length - 1];
                String filePath = "images" + File.separator + filename;
                Files.copy(file.toPath(), new File(filePath).toPath(), StandardCopyOption.REPLACE_EXISTING);
                iconRegister.setImage(new Image(new File(filePath).toURI().toString()));
                this.path = filePath;
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public void setAcc(AccountList accountList) {
        this.accountList = accountList;
    }

}
