package project.controllers;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import project.model.*;
import project.sevices.LetterFile;
import project.sevices.RoomFile;
import project.sevices.StringConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class ResidentController {
    private AccountList accountList;
    private LetterFile letterFile;
    private Account account;
    private Letter selectedLetter;
    private RoomFile roomFile;
    private RoomList roomList;
    private Room room;
    private MailList mailList;
    private ObservableList<Letter> lettersObservableList;

    @FXML
    private Label userNameLabel, nameLabel, senderNameLabel, recipientName, staffAddLabel, typeLabel, timeLabel;
    @FXML
    private TableView<Letter> letterNotReceivedTable;
    @FXML
    private Button logoutBtn, changePassBnt, mailReceivedBtn;
    @FXML
    private ImageView imagePath;

    @FXML
    private void initialize() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                userNameLabel.setText(accountList.getCurrentAccount().getUserName());
                nameLabel.setText(accountList.getCurrentAccount().getName());
                showLetterData();
            }
        });
        letterFile = new LetterFile("data");
        mailList = letterFile.getLetterData("Letter.csv");
        letterNotReceivedTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                showSelectedLetter(newValue);
            }
        });
    }

    private void showLetterData() {
        ArrayList<Letter> newList = new ArrayList<>();
        for (Letter letter : mailList.letterNotReceivedList()) {
            if (accountList.getCurrentAccount().getNumRoom().equals(letter.getRoomNumber())) {
                newList.add(letter);
            }
        }
        lettersObservableList = FXCollections.observableArrayList(newList);
        letterNotReceivedTable.setItems(lettersObservableList);

        ArrayList<StringConfiguration> configs = new ArrayList<>();
        configs.add(new StringConfiguration("title:ชื่อผู้ส่ง", "field:senderName"));
        configs.add(new StringConfiguration("title:ชื่อผู้รับ", "field:recipientName"));
        configs.add(new StringConfiguration("title:สถานะ", "field:status"));
        configs.add(new StringConfiguration("title:ขนาด", "field:size"));
        configs.add(new StringConfiguration("title:ประเภท", "field:type"));
        configs.add(new StringConfiguration("title:เลขห้อง", "field:roomNumber"));
        configs.add(new StringConfiguration("title:เวลาที่จดหมายเข้า", "field:timeIn"));

        for (StringConfiguration conf : configs) {
            TableColumn col = new TableColumn(conf.get("title"));
            col.setCellValueFactory(new PropertyValueFactory<>(conf.get("field")));
            col.prefWidthProperty().bind(letterNotReceivedTable.widthProperty().divide(7));
            letterNotReceivedTable.getColumns().add(col);
        }
    }

    private void showSelectedLetter(Letter letter) {
        selectedLetter = letter;
        senderNameLabel.setText(letter.getSenderName());
        recipientName.setText(letter.getRecipientName());
        staffAddLabel.setText(letter.getStaffAdd());
        typeLabel.setText(letter.getType());
        timeLabel.setText(letter.getTimeIn());
        imagePath.setImage(new Image(new File(selectedLetter.getImagePath()).toURI().toString()));
    }

    @FXML
    public void handleLogoutBtnOnAction(ActionEvent event) throws IOException {
        Button logoutBtn = (Button) event.getSource();
        Stage stage = (Stage) logoutBtn.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/login.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        ControllerLogin controllerLogin = loader.getController();
        controllerLogin.setAcc(accountList);

        stage.show();
    }

    @FXML
    public void handleChangePasswordBtnOnAction(ActionEvent event) throws IOException {
        Button changPassBtn = (Button) event.getSource();
        Stage stage = (Stage) changPassBtn.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/changepassword.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        ChangePasswordController changePasswordController = loader.getController();
        changePasswordController.setAcc(accountList);
        stage.show();
    }

    @FXML
    public void handleMailReceivedBtnOnAction(ActionEvent event) throws IOException {
        Button mailReceivedBtn = (Button) event.getSource();
        Stage stage = (Stage) mailReceivedBtn.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/mailReceivedResident.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        MailReceivedResidentController mailReceivedResidentController = loader.getController();
        mailReceivedResidentController.setAcc(accountList);
        stage.show();
    }

    public void setAcc(AccountList accountList) {
        this.accountList = accountList;
    }

}
