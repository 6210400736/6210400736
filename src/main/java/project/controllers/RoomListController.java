package project.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import project.model.*;
import project.sevices.RoomFile;
import project.sevices.StringConfiguration;
import project.strategy.ByPartialNameAndNumRoom;
import project.strategy.Searcher;

import java.io.IOException;
import java.util.ArrayList;

public class RoomListController {
    private RoomList roomList;
    private Room selectedRoom;
    private Room room;
    private RoomFile dataSource;
    private AccountList accountList;
    private ObservableList<Room> roomObservableList;
    private Searcher searcher;

    @FXML
    private Button backBtn, searchBtn, clearBtn;
    @FXML
    private TableView<Room> roomTable;
    @FXML
    private TextField searcherField;
    @FXML
    private Label residentNameLabel, numberRoomLabel, floorLabel, buildingLabel, roomTypeLabel, residentNameSecondLabel;

    @FXML
    public void initialize() {
        dataSource = new RoomFile("data");
        roomList = dataSource.getRoomsData("Room.csv");
        showRoomData();
        roomTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                showSelectedRoom(newValue);
            }
        });
        optionByPartialName();
    }

    private void showRoomData() {
        roomObservableList = FXCollections.observableArrayList(roomList.toList());
        roomTable.setItems(roomObservableList);

        ArrayList<StringConfiguration> configs = new ArrayList<>();
        configs.add(new StringConfiguration("title:ชื่อผู้อยู่อาศัย", "field:residentName"));
        configs.add(new StringConfiguration("title:ชื่อผู้อยู่อาศัยที่สอง", "field:residentNameSecond"));
        configs.add(new StringConfiguration("title:ชั้น", "field:floor"));
        configs.add(new StringConfiguration("title:เลขห้อง", "field:numberRoom"));
        configs.add(new StringConfiguration("title:ประเภทห้อง", "field:roomType"));
        configs.add(new StringConfiguration("title:อาคารที่", "field:building"));

        for (StringConfiguration conf : configs) {
            TableColumn col = new TableColumn(conf.get("title"));
            col.setCellValueFactory(new PropertyValueFactory<>(conf.get("field")));
            col.prefWidthProperty().bind(roomTable.widthProperty().divide(6));
            roomTable.getColumns().add(col);
        }
    }

    private void showSelectedRoom(Room room) {
        selectedRoom = room;
        residentNameLabel.setText(room.getResidentName());
        numberRoomLabel.setText(room.getNumberRoom());
        floorLabel.setText(room.getFloor());
        roomTypeLabel.setText(room.getRoomType());
        buildingLabel.setText(room.getBuilding());
        residentNameSecondLabel.setText(room.getResidentNameSecond());
    }

    private void clearSelectedRoom() {
        selectedRoom = null;
        residentNameLabel.setText("...");
        numberRoomLabel.setText("...");
        floorLabel.setText("...");
        roomTypeLabel.setText("...");
        buildingLabel.setText("...");
    }

    @FXML
    public void handleBackBtnOnAction(ActionEvent event) throws IOException {
        Button backBtn = (Button) event.getSource();
        Stage stage = (Stage) backBtn.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/staff.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        StaffController staffController = loader.getController();
        staffController.setRoom(roomList);
        StaffController staffController1 = loader.getController();
        staffController1.setAcc(accountList);
        stage.show();
    }

    private void optionByPartialName() {
        searcher = new ByPartialNameAndNumRoom();
        searcherField.textProperty().addListener((observable, oldValue, newValue) -> {
            {
                String residentName = newValue;
                ((ByPartialNameAndNumRoom) searcher).setResidentName(residentName);
            }
        });
    }

    @FXML
    public void handleSearchBtnOnAction(ActionEvent event) throws IOException {
        if (searcher != null) {
            updateTableView(roomList.search(searcher));
        } else {
            updateTableView(roomList.toList());
        }
    }

    private void updateTableView(ArrayList<Room> list) {
        roomTable.getItems().clear();
        for (Room room : list) {
            roomTable.getItems().add(room);
        }
    }

    @FXML
    public void handleClearBtnOnAction(ActionEvent event) throws IOException {
        roomObservableList = FXCollections.observableArrayList(roomList.toList());
        roomTable.setItems(roomObservableList);
    }


    public void setAcc(AccountList accountList) {
        this.accountList = accountList;
    }

    public void setRoom(RoomList roomList) {
        this.roomList = roomList;
    }
}
