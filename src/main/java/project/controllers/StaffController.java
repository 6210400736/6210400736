package project.controllers;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import project.model.*;
import project.sevices.LetterFile;
import project.sevices.RoomFile;
import project.sevices.StringConfiguration;
import project.strategy.ByPartialNameAndNumRoom;
import project.strategy.Searcher;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class StaffController {
    private Searcher searcher;
    private LetterFile dataSource;
    private Letter selectedLetter;
    private RoomFile roomFile;
    private RoomList roomList;
    private AccountList accountList;
    private MailList mailList;
    private Letter letter;
    private ObservableList<Letter> lettersObservableList;

    @FXML
    private Button logoutBtn, changePassBnt, roomListBtn, addLetterBtn, addRoomBtn, addResidentBtn, receivedBtn, mailReceivedBtn, searchBtn;
    @FXML
    private Label userNameStaff, senderNameLabel, recipientName, numberRoomLabel, typeLabel;
    @FXML
    private ImageView imageLetter, staffImage;
    @FXML
    private TableView<Letter> letterTable;
    @FXML
    private TextField searchField;
    @FXML
    private ComboBox<String> sortLetter;

    @FXML
    private void initialize() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                userNameStaff.setText(accountList.getCurrentAccount().getUserName());
                staffImage.setImage(new Image(new File(accountList.getCurrentAccount().getImagePath()).toURI().toString()));
            }
        });
        roomFile = new RoomFile("data");
        roomList = roomFile.getRoomsData("Room.csv");
        dataSource = new LetterFile("data");
        mailList = dataSource.getLetterData("Letter.csv");
        showLetterData();
        letterTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                showSelectedLetter(newValue);
            }
        });
        sortLetter.getItems().addAll("เรียงตามวันรับ", "เรียงตามเลขห้องมากไปน้อย");
        optionNumRoom();
    }

    private void showLetterData() {
        lettersObservableList = FXCollections.observableArrayList(mailList.letterNotReceivedList());
        letterTable.setItems(lettersObservableList);

        ArrayList<StringConfiguration> configs = new ArrayList<>();
        configs.add(new StringConfiguration("title:ชื่อผู้ส่ง", "field:senderName"));
        configs.add(new StringConfiguration("title:ชื่อผู้รับ", "field:recipientName"));
        configs.add(new StringConfiguration("title:สถานะ", "field:status"));
        configs.add(new StringConfiguration("title:ขนาด", "field:size"));
        configs.add(new StringConfiguration("title:ประเภท", "field:type"));
        configs.add(new StringConfiguration("title:เลขห้อง", "field:roomNumber"));
        configs.add(new StringConfiguration("title:เวลาที่จดหมายเข้า", "field:timeIn"));

        for (StringConfiguration conf : configs) {
            TableColumn col = new TableColumn(conf.get("title"));
            col.setCellValueFactory(new PropertyValueFactory<>(conf.get("field")));
            letterTable.getColumns().add(col);
        }
    }

    private void showSelectedLetter(Letter letter) {
        selectedLetter = letter;
        senderNameLabel.setText(selectedLetter.getSenderName());
        recipientName.setText(selectedLetter.getRecipientName());
        numberRoomLabel.setText(selectedLetter.getRoomNumber());
        typeLabel.setText(selectedLetter.getType());
        imageLetter.setImage(new Image(new File(selectedLetter.getImagePath()).toURI().toString()));
        receivedBtn.setDisable(false);
    }

    private void clearSelectedLetter() {
        selectedLetter = null;
        receivedBtn.setDisable(true);
    }

    @FXML
    public void handleLogoutBtnOnAction(ActionEvent event) throws IOException {
        Button logoutBtn = (Button) event.getSource();
        Stage stage = (Stage) logoutBtn.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/login.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        ControllerLogin controllerLogin = loader.getController();
        controllerLogin.setAcc(accountList);

        stage.show();
    }


    @FXML
    public void handleChangePasswordBtnOnAction(ActionEvent event) throws IOException {
        Button changPassBtn = (Button) event.getSource();
        Stage stage = (Stage) changPassBtn.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/changepassword.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        ChangePasswordController changePasswordController = loader.getController();
        changePasswordController.setAcc(accountList);
        stage.show();
    }

    @FXML
    public void handleAddLetterBtnOnAction(ActionEvent event) throws IOException {
        System.out.println(mailList.toList());
        Button addLetterBtn = (Button) event.getSource();
        Stage stage = (Stage) addLetterBtn.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/letter.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        LetterController letterController = loader.getController();
        letterController.setAcc(accountList);
        LetterController letterController1 = loader.getController();
        letterController1.setMailList(mailList);
        stage.show();
    }

    @FXML
    public void handleRoomListBtnOnAction(ActionEvent event) throws IOException {
        Button roomListBtn = (Button) event.getSource();
        Stage stage = (Stage) roomListBtn.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/roomlist.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        RoomListController roomListController = loader.getController();
        roomListController.setRoom(roomList);
        RoomListController roomListController2 = loader.getController();
        roomListController2.setAcc(accountList);
        stage.show();
    }

    @FXML
    public void handleAddRoomBtnOnAction(ActionEvent event) throws IOException {
        Button addLetterBtn = (Button) event.getSource();
        Stage stage = (Stage) addLetterBtn.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/addroom.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        AddRoomController addRoomController = loader.getController();
        addRoomController.setAcc(accountList);
        AddRoomController addRoomController1 = loader.getController();
        addRoomController1.setRoom(roomList);
        stage.show();
    }

    @FXML
    public void handleAddResidentBtnOnAction(ActionEvent event) throws IOException {
        Button addLetterBtn = (Button) event.getSource();
        Stage stage = (Stage) addLetterBtn.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/addresident.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        AddResidentController addResidentController = loader.getController();
        addResidentController.setAcc(accountList);
        AddResidentController addResidentController1 = loader.getController();
        addResidentController1.setRoom(roomList);
        stage.show();
    }

    @FXML
    public void handleMailReceivedBtnOnAction(ActionEvent event) throws IOException {
        Button addLetterBtn = (Button) event.getSource();
        Stage stage = (Stage) addLetterBtn.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/mailReceived.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        MailReceivedController mailReceivedController = loader.getController();
        mailReceivedController.setAcc(accountList);
        stage.show();
    }

    @FXML
    public void handleReceivedBtnOnAction(ActionEvent event) throws IOException {
        for (Letter letter : mailList.toList()) {
            if (letter.getStatus().equals("Not received")) {
                selectedLetter.setStatus("Received");
            }
        }
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
        String strDate = formatter.format(date);
        selectedLetter.setTimeOut(strDate);
        selectedLetter.setStaffPick(accountList.getCurrentAccount().getName());
        clearSelectedLetter();
        letterTable.refresh();
        letterTable.getSelectionModel().clearSelection();
        dataSource.setLetterData(mailList);
        lettersObservableList = FXCollections.observableArrayList(mailList.letterNotReceivedList());
        letterTable.setItems(lettersObservableList);
    }

    private void optionNumRoom() {
        searcher = new ByPartialNameAndNumRoom();
        searchField.textProperty().addListener((observable, oldValue, newValue) -> {
            {
                String numRoom = newValue;
                ((ByPartialNameAndNumRoom) searcher).setNumRoom(numRoom);
            }
        });
    }

    @FXML
    public void handleSearchBtnOnAction(ActionEvent event) throws IOException {
        if (searcher != null) {
            updateTableView(mailList.search(searcher));
        } else {
            updateTableView(mailList.toList());
        }
    }

    private void updateTableView(ArrayList<Letter> list) {
        letterTable.getItems().clear();
        for (Letter letter : list) {
            letterTable.getItems().add(letter);
        }
    }

    @FXML
    public void handleClearBtnOnAction(ActionEvent event) throws IOException {
        lettersObservableList = FXCollections.observableArrayList(mailList.letterNotReceivedList());
        letterTable.setItems(lettersObservableList);
    }

    @FXML
    public void SortOnAction(ActionEvent event) throws IOException {
        if (sortLetter.getValue().equals("เรียงตามวันรับ")) {
            lettersObservableList = FXCollections.observableArrayList(mailList.letterNotReceivedList());
            letterTable.setItems(lettersObservableList);

        }
        if (sortLetter.getValue().equals("เรียงตามเลขห้องมากไปน้อย")) {
            lettersObservableList = FXCollections.observableArrayList(mailList.letterNumRoomList());
            letterTable.setItems(lettersObservableList);
        }
    }

    public void setAcc(AccountList accountList) {
        this.accountList = accountList;
    }

    public void setRoom(RoomList roomList) {
        this.roomList = roomList;
    }

    public void setMailList(MailList mailList) {
        this.mailList = mailList;
    }

}
