package project.model;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class Account  {

    private String userName;
    private String name;
    private String email;
    private String password;
    private String type;
    private String status;
    private String onlineTime;
    private String imagePath;
    private String numRoom;
    private String login;


    public Account(String userName, String name , String email , String password, String type, String status , String onlineTime, String imagePath, String numRoom, String login)
    {
        this.userName = userName;
        this.name = name;
        this.email = email;
        this.password = password;
        this.type = type;
        this.status = status;
        this.onlineTime = onlineTime;
        this.imagePath = imagePath;
        this.numRoom = numRoom;
        this.login = login;
    }

    @Override
    public String toString() {
        return "Account{" +
                "userName='" + userName + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public String getUserName() {return userName;}

    public String getName() {return name;}

    public String getEmail() {return email;}

    public String getPassword()  {return password;}

    public String getType() {return type;}

    public String getStatus(){return status;}

    public String getOnlineTime() { return onlineTime; }

    public String getImagePath(){ return imagePath;}

    public String getNumRoom() { return numRoom;}

    public String getLogin() { return login; }

    public void setPassword(String password) {this.password = password;}

    public void setType(String type) {this.type = type;}

    public void setStatus(String status) { this.status = status; }

    public void setOnlineTime(String onlineTime) { this.onlineTime = onlineTime; }

    public void setImagePath(String path){ this.imagePath = path; }

    public void setNumRoom(String numRoom) { this.numRoom = numRoom; }

    public void setLogin(String login) { this.login = login; }

}

