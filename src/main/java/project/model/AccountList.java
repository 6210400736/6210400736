package project.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class AccountList  {

    private ArrayList<Account> accounts;
    private Account currentAccount;

    public  AccountList()
    {
        accounts = new ArrayList<>();
    }

    public void addAccount(Account acc)
    {
        accounts.add(acc);
    }

    public boolean checkPasswordAndUserName(String password, String userName)
    {

        for(Account acc : accounts)
        {
            if(acc.getPassword().equals(password) && acc.getUserName().equals(userName)){
                currentAccount = acc;
                return true;
            }
        }

        currentAccount = null;
        return false;
    }


    public Account getCurrentAccount()
    {
        return currentAccount;
    }

    @Override
    public String toString() {
        return "account" +  accounts;
    }

    public ArrayList<Account> toList() {
        return accounts;
    }

    public ArrayList<Account> toStaffList(){
        ArrayList<Account> staffList = new ArrayList<>();
        for(Account account : accounts)
        {
            if(account.getType().equals("Staff")){
                staffList.add(account);
            }
        }Collections.sort(staffList, new Comparator<Account>() {
            @Override
            public int compare(Account o1, Account o2) {
                return  o2.getOnlineTime().compareTo(o1.getOnlineTime());
            }
        });
        return staffList;
    }


}
