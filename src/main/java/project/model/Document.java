package project.model;

public class Document extends Letter{
    private String importance;

    public Document(String senderName, String recipientName, String roomNumber, String status, String size, String type, String timeIn, String timeOut, String imagePath, String staffPick, String StaffAdd, String importance) {
        super(senderName, recipientName, roomNumber, status, size, type, timeIn, timeOut, imagePath, staffPick, StaffAdd);
        this.importance = importance;
    }
    public String getImportance(){return importance;}

}
