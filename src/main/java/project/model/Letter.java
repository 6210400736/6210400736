package project.model;

public class Letter {
    private String senderName;
    private String recipientName;
    private String roomNumber;
    private String status;
    private String size;
    private String type;
    private String timeIn;
    private String timeOut;
    private String imagePath;
    private String staffPick;
    private String staffAdd;

    public Letter(String senderName, String recipientName, String roomNumber, String status, String size, String type, String timeIn , String timeOut, String imagePath, String staffPick, String staffAdd)
    {
        this.senderName =  senderName;
        this.recipientName = recipientName;
        this.roomNumber = roomNumber;
        this.status = status;
        this.size = size;
        this.type = type;
        this.timeIn = timeIn;
        this.timeOut = timeOut;
        this.imagePath = imagePath;
        this.staffPick = staffPick;
        this.staffAdd = staffAdd;
    }

    public String getSenderName() {
        return senderName;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public String  getStatus(){return status; }

    public String getType(){return  type;}

    public String getSize() {return size;}

    public String getTimeIn() {return timeIn;}

    public String getTimeOut() {return timeOut;}

    public  String getImagePath(){ return imagePath;}

    public String getStaffPick() { return staffPick;}

    public String getStaffAdd() { return staffAdd; }

    public void setSize(String size){ this.size = size;}

    public void setType(String type){this.type = type;}

    public void setStatus(String status) {
        this.status = status;
    }

    public void setTimeIn(String timeIn){this.timeIn = timeIn;}

    public void setTimeOut(String timeOut){this.timeOut = timeOut;}

    public void setImagePath(String path){ this.imagePath = path; }

    public void setStaffPick(String staffPick) { this.staffPick = staffPick; }

    public void setStaffAdd(String staffAdd) { this.staffAdd = staffAdd;}
}
