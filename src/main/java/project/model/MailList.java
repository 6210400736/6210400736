package project.model;

import project.strategy.Searcher;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

public class MailList {

    private ArrayList<Letter> mail;

    public  MailList() { mail = new ArrayList<>(); }

    public void addMailLetter(Letter letter) { mail.add(letter); }

    public void addMailDocument(Document document) { mail.add(document); }

    public void addMailSupplies(Supplies supplies) { mail.add(supplies); }

    public ArrayList<Letter> toList() { return mail; }

    public ArrayList<Letter> letterReceivedList(){
        ArrayList<Letter> letterReceivedList = new ArrayList<>();
        for(Letter letter : mail)
        {
            if(letter.getStatus().equals("Received")){
                letterReceivedList.add(letter);
            }
        }Collections.sort(letterReceivedList, new Comparator<Letter>() {
            @Override
            public int compare(Letter o1, Letter o2) {
                return o2.getTimeOut().compareTo(o1.getTimeOut());
            }
        });
        return  letterReceivedList;
    }

    public ArrayList<Letter> letterNotReceivedList(){
        ArrayList<Letter> letterNotReceivedList = new ArrayList<>();
        for(Letter letter : mail)
        {
            if(letter.getStatus().equals("Not received")){
                letterNotReceivedList.add(letter);
            }
        }Collections.sort(letterNotReceivedList, new Comparator<Letter>() {
            @Override
            public int compare(Letter o1, Letter o2) {
                return o2.getTimeIn().compareTo(o1.getTimeIn());
            }
        });
        return  letterNotReceivedList;
    }

    public ArrayList<Letter> letterNumRoomList(){
        ArrayList<Letter> letterNumRoomList = new ArrayList<>();
        for(Letter letter : mail)
        {
            if(letter.getStatus().equals("Not received")){
                letterNumRoomList.add(letter);
            }
        }Collections.sort(letterNumRoomList, new Comparator<Letter>() {
            @Override
            public int compare(Letter o1, Letter o2) {
                return o2.getRoomNumber().compareTo(o1.getRoomNumber());
            }
        });
        return  letterNumRoomList;
    }

    public ArrayList<Letter> room(Room room) {
        ArrayList<Letter> newList = new ArrayList<>();
        for (Letter letter: mail) {
            if (letter.getRoomNumber().equals(room.getNumberRoom())) {
                newList.add(letter);
            }
        }
        return newList;
    }
    public ArrayList<Letter> search(Searcher searcher) {
        ArrayList<Letter> newList = new ArrayList<>();
        for (Letter letter: mail) {
            if (searcher.isMatchLetter(letter) && letter.getStatus().equals("Not received")) {
                newList.add(letter);
            }
        }
        return newList;
    }


    @Override
    public String toString() {
        return "mail" +  mail;
    }

}
