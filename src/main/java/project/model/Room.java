package project.model;

public class Room {
    private String floor;
    private String numberRoom;
    private String residentName;
    private String residentNameSecond;
    private String roomType;
    private String building;

    public Room(String residentName, String residentNameSecond, String floor, String numberRoom, String roomType, String building) {
        this.residentName = residentName;
        this.residentNameSecond = residentNameSecond;
        this.floor = floor;
        this.numberRoom = numberRoom;
        this.roomType = roomType;
        this.building = building;
    }

    public String getFloor(){return floor;}

    public String getNumberRoom(){return numberRoom;}

    public String getResidentName(){return residentName;}

    public String getRoomType(){return roomType;}

    public String getBuilding() { return building;}

    public String getResidentNameSecond() {return residentNameSecond;}

    public void setFloor(String floor){this.floor = floor;}

    public void setNumberRoom(String numberRoom){this.numberRoom = numberRoom;}

    public void setResidentNameSecond(String residentNameSecond){this.residentNameSecond = residentNameSecond;}

    public void setResidentName(String residentName){this.residentName = residentName;}

    public void setRoomType(String roomType){this.roomType = roomType;}

}
