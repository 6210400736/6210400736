package project.model;

import project.strategy.Searcher;

import java.util.ArrayList;

public class RoomList {
    private ArrayList<Room> rooms;

    public  RoomList() { rooms = new ArrayList<>(); }

    public void addRoom(Room room) { rooms.add(room); }

    public ArrayList<Room> toList() { return rooms; }

    @Override
    public String toString() {
        return "room" +  rooms;
    }

    public ArrayList<Room> search(Searcher searcher) {
        ArrayList<Room> newList = new ArrayList<>();
        for (Room room: rooms) {
            if (searcher.isMatchRoom(room)) {
                newList.add(room);
            }
        }
        return newList;
    }
}
