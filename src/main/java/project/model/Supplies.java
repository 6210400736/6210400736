package project.model;

public class Supplies extends Letter{
    private String  trackingNumber;
    private String  deliveryService;

    public Supplies(String senderName, String recipientName, String roomNumber, String status, String size, String type, String timeIn, String timeOut, String imagePath, String staffPick, String StaffAdd, String trackingNumber, String deliveryService) {
        super(senderName, recipientName, roomNumber, status, size, type, timeIn, timeOut, imagePath, staffPick, StaffAdd);
        this.trackingNumber = trackingNumber;
        this.deliveryService =  deliveryService;
    }
    public String getTrackingNumber(){return trackingNumber;}

    public String getDeliveryService(){return deliveryService;}

}
