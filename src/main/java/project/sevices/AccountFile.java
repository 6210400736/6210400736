package project.sevices;

import project.model.Account;
import project.model.AccountList;

import java.io.*;

public class AccountFile {

    private String fileDirectoryName;
    private String accountFile;
    private AccountList accountList;

    public AccountFile(String fileDirectoryName) {
        this.fileDirectoryName = fileDirectoryName;
        this.accountFile = "Account.csv";
        checkFileIsExisted(accountFile);
    }

    private void checkFileIsExisted(String fileName) {
        File file = new File(fileDirectoryName);
        if (!file.exists()) {
            file.mkdirs();
        }
        String filePath = fileDirectoryName + File.separator + fileName;
        file = new File(filePath);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.err.println("Cannot create " + filePath);
            }
        }
    }

    private void readData(String fileName) throws IOException {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileReader fileReader = new FileReader(file);
        BufferedReader reader = new BufferedReader(fileReader);
        String line = "";
        while ((line = reader.readLine()) != null) {
            String[] data = line.split(",");
            Account account = new Account(data[0].trim(), data[1].trim(), data[2].trim(), data[3].trim(), data[4].trim(), data[5].trim(), data[6].trim(), data[7].trim(), data[8].trim(), data[9].trim());
            accountList.addAccount(account);
        }
        reader.close();
    }

    public AccountList getAccountsData(String fileName) {
        try {
            accountList = new AccountList();
            readData(fileName);
        } catch (FileNotFoundException e) {
            System.err.println(this.fileDirectoryName + " not found");
        } catch (IOException e) {
            System.err.println("IOException from reading " + this.fileDirectoryName);
        }
        return accountList;
    }


    public void setAccountData(AccountList accountList) {
        String filePath = fileDirectoryName + File.separator + accountFile;
        File file = new File(filePath);
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(file);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            for (Account account : accountList.toList()) {
                String line = account.getUserName() + ","
                        + account.getName() + ","
                        + account.getEmail() + ","
                        + account.getPassword() + ","
                        + account.getType() + ","
                        + account.getStatus() + ","
                        + account.getOnlineTime() + ","
                        + account.getImagePath() + ","
                        + account.getNumRoom() + ","
                        + account.getLogin();
                writer.append(line);
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            System.err.println("Cannot write " + filePath);
        }
    }

    public void accountList(AccountList accountList) {
        for (Account account : this.getAccountsData("Account.csv").toList()) {
            accountList.addAccount(account);
        }

    }
}
