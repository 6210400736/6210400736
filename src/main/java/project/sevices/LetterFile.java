package project.sevices;

import project.model.*;

import java.io.*;

public class LetterFile {
        private String fileDirectoryName;
        private String letterFile;
        private MailList mailList;

        public LetterFile(String fileDirectoryName) {
            this.fileDirectoryName = fileDirectoryName;
            this.letterFile = "letter.csv";
            checkFileIsExisted(letterFile);
        }

        private void checkFileIsExisted(String fileName) {
            File file = new File(fileDirectoryName);
            if (!file.exists()) {
                file.mkdirs();
            }
            String filePath = fileDirectoryName + File.separator + fileName;
            file = new File(filePath);
            if (!file.exists()) {
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    System.err.println("Cannot create " + filePath);
                }
            }
        }

        private void readDataLetter(String fileName) throws IOException {
            String filePath = fileDirectoryName + File.separator + fileName;
            File file = new File(filePath);
            FileReader fileReader = new FileReader(file);
            BufferedReader reader = new BufferedReader(fileReader);
            String line = "";
            while ((line = reader.readLine()) != null) {
                String[] data = line.split(",");
                if(data[5].equals("Letter")) {
                    Letter letter = new Letter(data[0].trim(), data[1].trim(), data[2].trim(), data[3].trim(), data[4].trim(), data[5].trim(), data[6].trim(), data[7].trim(), data[8].trim(), data[9].trim(), data[10].trim());
                    mailList.addMailLetter(letter);
                }
                else if(data[5].equals("Document")){
                    Document document = new Document(data[0].trim(), data[1].trim(), data[2].trim(), data[3].trim(), data[4].trim(), data[5].trim(), data[6].trim(), data[7].trim(), data[8].trim(), data[9].trim(), data[10].trim(), data[11].trim());
                    mailList.addMailLetter(document);
                }
                else if(data[5].equals("Supplies")){
                    Supplies supplies = new Supplies(data[0].trim(), data[1].trim(), data[2].trim(), data[3].trim(), data[4].trim(), data[5].trim(), data[6].trim(), data[7].trim(), data[8].trim(), data[9].trim(), data[10].trim(), data[11].trim(), data[12].trim());
                    mailList.addMailLetter(supplies);
                }

            }
            reader.close();
        }

        public MailList getLetterData(String fileName) {
            try {
                mailList = new MailList();
                readDataLetter(fileName);
            } catch (FileNotFoundException e) {
                System.err.println(this.fileDirectoryName + " not found");
            } catch (IOException e) {
                System.err.println("IOException from reading " + this.fileDirectoryName);
            }
            return mailList;
        }


    public void setLetterData(MailList mailList) {
            String filePath = fileDirectoryName + File.separator + letterFile;
            File file = new File(filePath);
            FileWriter fileWriter = null;
            try {
                fileWriter = new FileWriter(file);
                BufferedWriter writer = new BufferedWriter(fileWriter);
                for (Letter letter : mailList.toList()) {
                    if(letter.getType().equals("Letter")) {
                        String line = letter.getSenderName() + ","
                                + letter.getRecipientName() + ","
                                + letter.getRoomNumber() + ","
                                + letter.getStatus() + ","
                                + letter.getSize() + ","
                                + letter.getType() + ","
                                + letter.getTimeIn() + ","
                                + letter.getTimeOut() + ","
                                + letter.getImagePath() + ","
                                + letter.getStaffPick() + ","
                                + letter.getStaffAdd();
                        writer.append(line);
                        writer.newLine();
                    }
                    else  if (letter.getType().equals("Document")){
                        String line = letter.getSenderName() + ","
                                + letter.getRecipientName() + ","
                                + letter.getRoomNumber() + ","
                                + letter.getStatus() + ","
                                + letter.getSize() + ","
                                + letter.getType() + ","
                                + letter.getTimeIn() + ","
                                + letter.getTimeOut() + ","
                                + letter.getImagePath() + ","
                                + letter.getStaffPick() + ","
                                + letter.getStaffAdd() +  ","
                                + ((Document)letter).getImportance();
                        writer.append(line);
                        writer.newLine();
                    }
                    else if (letter.getType().equals("Supplies"))
                    {
                        String line = letter.getSenderName() + ","
                                + letter.getRecipientName() + ","
                                + letter.getRoomNumber() + ","
                                + letter.getStatus() + ","
                                + letter.getSize() + ","
                                + letter.getType() + ","
                                + letter.getTimeIn() + ","
                                + letter.getTimeOut() + ","
                                + letter.getImagePath() + ","
                                + letter.getStaffPick() + ","
                                + letter.getStaffAdd() +  ","
                                + ((Supplies)letter).getTrackingNumber() + ","
                                + ((Supplies)letter).getDeliveryService();
                        writer.append(line);
                        writer.newLine();
                    }
                }
                writer.close();
            } catch (IOException e) {
                System.err.println("Cannot write " + filePath);
            }
        }

}
