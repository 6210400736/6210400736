package project.sevices;

import project.model.Account;
import project.model.AccountList;
import project.model.Room;
import project.model.RoomList;

import java.io.*;

public class RoomFile {
    private String fileDirectoryName;
    private String roomFile;
    private RoomList roomList;

    public RoomFile(String fileDirectoryName) {
        this.fileDirectoryName = fileDirectoryName;
        this.roomFile = "Room.csv";
        checkFileIsExisted(roomFile);
    }

    private void checkFileIsExisted(String fileName) {
        File file = new File(fileDirectoryName);
        if (!file.exists()) {
            file.mkdirs();
        }
        String filePath = fileDirectoryName + File.separator + fileName;
        file = new File(filePath);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.err.println("Cannot create " + filePath);
            }
        }
    }

    private void readRoomData(String fileName) throws IOException {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileReader fileReader = new FileReader(file);
        BufferedReader reader = new BufferedReader(fileReader);
        String line = "";
        while ((line = reader.readLine()) != null) {
            String[] data = line.split(",");
            Room room = new Room(data[0].trim(), data[1].trim(), data[2].trim(), data[3].trim(), data[4].trim(), data[5].trim());
            roomList.addRoom(room);
        }
        reader.close();
    }

    public RoomList getRoomsData(String fileName) {
        try {
            roomList = new RoomList();
            readRoomData(fileName);
        } catch (FileNotFoundException e) {
            System.err.println(this.fileDirectoryName + " not found");
        } catch (IOException e) {
            System.err.println("IOException from reading " + this.fileDirectoryName);
        }
        return roomList;
    }


    public void setRoomData(RoomList roomList) {
        String filePath = fileDirectoryName + File.separator + roomFile;
        File file = new File(filePath);
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(file);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            for (Room room : roomList.toList()) {
                String line = room.getResidentName() + ","
                        + room.getResidentNameSecond() + ","
                        + room.getFloor() + ","
                        + room.getNumberRoom() + ","
                        + room.getRoomType() + ","
                        + room.getBuilding();
                writer.append(line);
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            System.err.println("Cannot write " + filePath);
        }
    }

    public void roomList(RoomList roomList) {
        for (Room room : this.getRoomsData("Room.csv").toList()) {
            roomList.addRoom(room);
        }
    }
}
