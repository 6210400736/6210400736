package project.strategy;

import project.model.Letter;
import project.model.Room;


public class ByPartialNameAndNumRoom implements Searcher{
    private String residentName;
    private String numRoom;

    public void setResidentName(String residentName){this.residentName = residentName;}
    public void setNumRoom(String numRoom){this.numRoom = numRoom;}

    @Override
    public boolean isMatchLetter(Letter letter) {
        return letter.getRoomNumber().contains(numRoom);
    }

    @Override
    public boolean isMatchRoom(Room room) {
        return room.getResidentName().toLowerCase().contains(residentName.toLowerCase());
    }
}
