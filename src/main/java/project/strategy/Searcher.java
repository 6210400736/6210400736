package project.strategy;

import project.model.Letter;
import project.model.Room;

public interface Searcher {

    boolean isMatchLetter(Letter letter);

    boolean isMatchRoom(Room room);
}
